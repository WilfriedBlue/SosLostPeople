<?php 

// src/AppBundle/Form/TaskType.php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateHelperForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName', null, array('label' => "add_helper.form.lastname" ))
            ->add('firstName', null, array('label' => "add_helper.form.firstname"))
            ->add('phone', null, array('label' => "add_helper.form.phone"))
            ->add('dateOfBirth', null, array('label' => "add_helper.form.date_of_birth"))
            ->add('company', null, array('label' => "add_helper.form.company"))
            ->add('job', null, array('label' => "add_helper.form.job"))
            ->add('email', null, array('label' => "add_helper.form.email"))
            ->add('save', SubmitType::class)
        ;
    }
}