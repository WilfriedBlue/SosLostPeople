<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;
use Symfony\Component\HttpFoundation\Response;
use Twilio\Twiml;
use Twilio\TwiML\VoiceResponse;
use AppBundle\Service\TwilioService;
use AppBundle\Service\AlertService;
use AppBundle\Service\LogService;
use AppBundle\Entity\Call;
use AppBundle\Entity\Alert;
use AppBundle\Entity\Human;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Form\CreateHelperForm;
use AppBundle\Form\CreateUserDringForm;



class DefaultController extends Controller
{
    /**
     * @Route("/admin/", name="base")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/admin/homepage", name="homepage")
     */
    public function homepage(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));
        $lastTenAlerts = $entity_manager_interface->getRepository(Alert::class)->getLastTenAlerts();
        $all_alerts = $entity_manager_interface->getRepository(Alert::class)->findAll();
    
        $last_alert_time = $lastTenAlerts[0]->getStartDate()->format('Y-m-d H:i:s');
        $date_now = time();
        $diff_time_alert  = abs($date_now - strtotime($last_alert_time));

        $close_alert_date = $lastTenAlerts[0]->getCloseAlertDate();

        $table_count_alert = [];
        $years = [];
        $months = [];
        
        for ($i=0; $i<=12; $i++) { 
            $year = date('Y', strtotime("-$i month"));
            $years[] = $year;
            $month = date('m', strtotime("-$i month"));
            $months[] = $month;
            $table_count_alert[] = $entity_manager_interface->getRepository(Alert::class)->countAlertsAtMonth($year, $month, date('t',mktime(0, 0, 0, date('m', strtotime("-$i month")), 1, date('Y', strtotime("-$i month")))));   
        }
        
        return $this->render('default/homepage.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'lastTenAlerts' => $lastTenAlerts,
            'diff_time_alert' => $diff_time_alert,
            'close_alert_date' => $close_alert_date,  
            'table_count_alert' => $table_count_alert,
            'years' => $years,
            'months' => $months,
            'all_alerts' => $all_alerts
            ]);
    }

    /**
     * @Route("/admin/list_users_dring", name="list_users_dring")
     */
    public function list_users_dring(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $users_dring = $entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER");
        

        return $this->render('default/list_users_dring.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'users_dring' => $users_dring  
            ]);
    }

    /**
     * @Route("/admin/list_alerts", name="list_alerts")
     */
    public function list_alert(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $alerts = $entity_manager_interface->getRepository(Alert::class)->findAll();
        

        return $this->render('default/list_alerts.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'alerts' => $alerts  
            ]);
    }

    /**
     * @Route("/admin/list_guardians", name="list_guardians")
     */
    public function list_guardian(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $guardians = $entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_GUARDIAN");
        

        return $this->render('default/list_guardians.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'guardians' => $guardians
            ]);
    }

    /**
     * @Route("/admin/list_helpers", name="list_helpers")
     */
    public function list_helpers(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $helpers = $entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER");
        

        return $this->render('default/list_helpers.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'helpers' => $helpers
            ]);
    }

    /**
     * @Route("/admin/add_helper", name="add_helper")
     */
    public function add_helper(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $helper = new Human();
        $form = $this->createForm(CreateHelperForm::class, $helper);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helper = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $helper->addRole("ROLE_HELPER");
            $entityManager->persist($helper);
            $entityManager->flush();

            return $this->redirectToRoute('list_helpers');
        }

        return $this->render('default/add_helper.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'create_helper_form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/admin/add_user_dring", name="add_user_dring")
     */
    public function add_user_dring(Request $request, EntityManagerInterface $entity_manager_interface, LogService $log_service)
    {
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $user_dring = new Human();
        $form = $this->createForm(CreateUserDringForm::class, $user_dring);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user_dring = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $user_dring->addRole("ROLE_DRINGUSER");
            $entityManager->persist($user_dring);
            $entityManager->flush();

            return $this->redirectToRoute('list_users_dring');
        }

        return $this->render('default/add_user_dring.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'create_user_dring_form' => $form->createView(),
            ]);
    }
    
    /**
     * @Route("/create_alert/{id}", name="create_alert")
     */
    public function create_alert(Request $request, EntityManagerInterface $entity_manager_interface, AlertService $alertService, $id)
    {
        $alertService->create_alert($id);
        $total_alerts = $entity_manager_interface->getRepository(Alert::class)->countAlerts();
        $total_alerts_validated = $entity_manager_interface->getRepository(Alert::class)->countAlertsValidated();
        $total_helpers = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER"));
        $total_dring_users = count($entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER"));

        $users_dring = $entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER");
        
        return $this->render('default/list_users_dring.html.twig', [
            'total_alerts' => $total_alerts,
            'total_alerts_validated' => $total_alerts_validated,
            'total_helpers' => $total_helpers,
            'total_dring_users' => $total_dring_users,
            'users_dring' => $users_dring  
            ]);
    }

    /**
     * @Route("/ask_question/{id}/{repeat}", name="answer")
     */
    public function answer(Request $request, AlertService $alertService, $id, $repeat)
    {
        $alertService->ask_question($id, $repeat);
        return new Response();
    }

    /**
     * @Route("/process_response/{id}/{repeat}", name="process_response")
     */
    public function process_response(Request $request, AlertService $alertService, $id, $repeat)
    {
        $alertService->process_response($id, $repeat);
        return new Response();
    }

    /**
     * @Route("/callback", name="callback")
     */
    public function callback(Request $request, AlertService $alertService, LogService $log_service, EntityManagerInterface $entity_manager_interface)
    {
        $call = $entity_manager_interface->getRepository(Call::class)->findOneByTwilioReference($_POST["CallSid"]);

        if ($_POST['CallStatus'] == "completed" || $_POST['CallStatus'] == "busy") {

            $alert = $call->getAlert();
            $helpers = $alert->getHelpers();
            $calls = $alert->getcalls();

            $alert->setCallFinishDate(new \DateTime());
            $call->setEndCallDate(new \DateTime()); 

            $entity_manager_interface->persist($alert);
            $entity_manager_interface->persist($call);
            
            $entity_manager_interface->flush();
            
            $alert = $call->getAlert();
            $helpers = $alert->getHelpers();
            $calls = $alert->getcalls();

            $number_of_calls_finished = $entity_manager_interface->getRepository(Call::class)->countEndCalls($alert->getId());
    
            $log_service->log("Nombre d'aidants pour ma condition: ".count($helpers));
            $log_service->log("Nombre d'appel pour ma condition: ".$number_of_calls_finished);
                
            $best_answer = 6;
                
            if ( count($helpers) == $number_of_calls_finished) {
                
                foreach ($calls as $call) {
                    if ($call->getAnswer() < $best_answer & $call->getAnswer() >= 2) {
                        $best_answer = $call->getAnswer();   
                    }
                    else if ($call->getAnswer() == 1) {
                        $best_answer = 1;
                    }
                }
                $log_service->log("Ma meilleures réponse est: ".json_encode($best_answer));

                if ( $best_answer == 6) {
                    $alertService->send_sms_when_no_helpers_answered($alert);
                }
                else {
                    $log_service->log("Ma meilleures réponse est: ".json_encode($best_answer));
                    $helpers_with_bestAnswer = [];

                    foreach ($calls as $call) {
                        if ( $best_answer == $call->getAnswer() ) {
                            $log_service->log("Ma best answer est egal à la réponse de l'aidant");
                            $helpers_with_bestAnswer[] = $call->getCalledHuman();
                        }
                        $log_service->log("Mes aidants ayant la meilleur réponse: ".json_encode($helpers_with_bestAnswer));
                    }
                    $alertService->send_sms_to_helpers_with_bestAnswer($helpers_with_bestAnswer, $alert);
                }
            }         
        }
        return new Response();
    }
}


    
