<?php

namespace AppBundle\Repository;

/**
 * CallRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CallRepository extends \Doctrine\ORM\EntityRepository
{
    public function countEndCalls($alert) {
    
    $qb = $this->createQueryBuilder('c')
    ->select('count(c.endCallDate)')
    ->leftJoin('c.alert', 'a')
    ->where('a.id = :id')
    ->setParameter('id', $alert);
    // ->getScalarResult() int result count
    return $qb->getQuery()->getSingleScalarResult();
    }    
}
