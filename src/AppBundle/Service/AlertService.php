<?php 

namespace AppBundle\Service;

use Twilio\Rest\Client;
use Symfony\Component\HttpFoundation\Response;
use Twilio\Twiml;
use Twilio\TwiML\VoiceResponse;
use AppBundle\Service\LogService;
use AppBundle\Service\TwilioService;
//use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Call;
use AppBundle\Entity\Human;
use AppBundle\Entity\Alert;
use AppBundle\Entity\Sms;


class AlertService { 
    private $accountSid;
    private $auth_token;
    private $twilio_number;
    private $app_url;
    private $log_service;
    private $entity_manager_interface;
    private $twilio_service;

    public function __construct($accountSid, $auth_token, $twilio_number, $app_url, LogService $log_service, EntityManagerInterface $entity_manager_interface, TwilioService $twilio_service) {
        $this->accountSid = $accountSid;
        $this->auth_token = $auth_token;
        $this->twilio_number = $twilio_number;
        $this->app_url = $app_url;
        $this->log_service = $log_service;
        $this->entity_manager_interface = $entity_manager_interface;
        $this->twilio_service = $twilio_service;
    }


    public function create_alert() {   
        $dringUsers = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_DRINGUSER");
        
        foreach ($dringUsers as $dringUser) {
        }
        $this->alert($dringUser);
    }    

    public function alert($dringUser){   
        $alert = new Alert;
        $alert->setStartDate(new \DateTime());

        $guardian = $alert->getGuardian();

         // Link alert with victim
         $alert->setVictim($dringUser);

        $this->entity_manager_interface->persist($alert);

        $helpers = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_HELPER");

        foreach ($helpers as $helper) {
            // Link one helper with one alert
            $helper->addAlertsWhereHelper($alert);
            $this->entity_manager_interface->persist($helper);
           
            $this->start_call($helper, $alert);
        }
        $this->entity_manager_interface->flush();
        $this->send_sms_when_start_alert($alert,$dringUser);
    }
    
    public function start_call($helper, $alert) 
{
        $this->twilio_service->call($helper, $alert);
    }    

    public function ask_question($id, $repeat) {
        $response = new VoiceResponse();

        $gather = $response->gather(['numDigits' => 1,
                                     'action' => $this->app_url."/process_response/".$id."/".$repeat,
                                     'method' => 'POST']);
        $gather->say(
            "Un technicien est en danger. Vous savez où il est, tapez 1. Vous l''avez vu il y a 5 minutes, tapez 2. Vous l'avez vu il y 10 minutes tapez 3. 
            Vous l'avez vu il y a 15 minute tapez 4. Vous l'avez vu il y a 30 minutes tapez 5.",
            array("voice" => "woman", "language" => "fr-FR")
        );

        echo ($response);

        return new Response();
    }

    public function process_response($id, $repeat) {
        $response = new VoiceResponse();

        $call = $this->entity_manager_interface->getRepository(Call::class)->find($id);
        $call->setAnswerCallDate(new \DateTime());
    
        $alert = $call->getAlert();
        $helpers = $alert->getHelpers();
        $calls = $alert->getCalls();

        $digits = isset($_POST["Digits"]) ? $_POST["Digits"] : "";
        $call->setAnswer($digits);

        // condition for loop message one time
        if ($repeat != 1) {
            if ($call->getAnswer() == 0 || $call->getAnswer() > 5) {
                $repeat = 1;
                $response->redirect($this->app_url."/ask_question/".$id."/".$repeat, ['method' => 'POST']);
            }
        }
        if ($call->getAnswer() == 0 || $call->getAnswer() > 5) {
            $response->say(
                "Mauvaise réponse",
                array("voice" => "woman", "language" => "fr-FR")
            );    
        } 
        else {
            $response->say(
                "Merci pour votre réponse",
                array("voice" => "woman", "language" => "fr-FR")
            );
        }

        $this->entity_manager_interface->persist($call);
        $this->entity_manager_interface->flush();
        
        if ($call->getAnswer() == 1 ) {
            $this->send_sms_when_answer_one($call->getCalledHuman(), $alert);
        }
          
        header('Content-Type: text/xml');

        echo $response;

        return new Response();
    }

    public function send_sms_when_answer_one(Human $helper, Alert $alert) {   
        $guardians = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_GUARDIAN");
        $body= "Sos-Lost-People Tél: ".$helper->getPhone()." Nom: ".$helper->getLastName()." Prénom: ".$helper->getFirstName();
        foreach ($guardians as $guardian) {
            // Link alert with guardian
            $alert->setGuardian($guardian);
            $this->twilio_service->send_sms($alert, $body, $guardian);
        }
        
    }

    public function send_sms_to_helpers_with_bestAnswer($helpers_with_bestAnswer, Alert $alert) {   
        $body = "";
        foreach ($helpers_with_bestAnswer as $helper_with_bestAnswer) {
            $helper_fistName = $helper_with_bestAnswer->getFirstName();

            $helper_lastName = $helper_with_bestAnswer->getLastName();

            $helper_phone = $helper_with_bestAnswer->getPhone();

            $body .= "Sos-Lost-People Tél: ".$helper_phone." Nom: ".$helper_lastName." Prénom: ".$helper_fistName;
        }
        $guardians = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_GUARDIAN");
        foreach ($guardians as $guardian) {
            // Link alert with guardian
            $alert->setGuardian($guardian);
            $this->twilio_service->send_sms($alert, $body, $guardian);
        } 
    }

    public function send_sms_when_no_helpers_answered($alert) {   
        $guardians = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_GUARDIAN");
        $body= "Sos-Lost-People Aucun aidant n'est disponible";
        foreach ($guardians as $guardian) {
            // Link alert with guardian
            $alert->setGuardian($guardian);
            $this->twilio_service->send_sms($alert, $body, $guardian);
        }
    }

    public function send_sms_when_start_alert($alert,$dringUser) {   
        $guardians = $this->entity_manager_interface->getRepository(Human::class)->findByRole("ROLE_GUARDIAN");
        $body= "Sos-Lost-People Alerte " .$dringUser->getLastName()." ".$dringUser->getFirstName(). " vient de tombé. Tél: " .$dringUser->getPhone();
        foreach ($guardians as $guardian) {
            // Link alert with guardian
            $alert->setGuardian($guardian);
            $this->twilio_service->send_sms($alert, $body, $guardian);
        }   
    }  
}