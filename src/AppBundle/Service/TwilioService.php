<?php 

namespace AppBundle\Service;

use Twilio\Rest\Client;
use Symfony\Component\HttpFoundation\Response;
use Twilio\Twiml;
use Twilio\TwiML\VoiceResponse;
use AppBundle\Service\LogService;
//use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Call;
use AppBundle\Entity\Human;
use AppBundle\Entity\Alert;
use AppBundle\Entity\Sms;


class TwilioService { 
    private $accountSid;
    private $auth_token;
    private $twilio_number;
    private $app_url;
    private $log_service;
    private $entity_manager_interface;

    public function __construct($accountSid, $auth_token, $twilio_number, $app_url, LogService $log_service, EntityManagerInterface $entity_manager_interface) {
        $this->accountSid = $accountSid;
        $this->auth_token = $auth_token;
        $this->twilio_number = $twilio_number;
        $this->app_url = $app_url;
        $this->log_service = $log_service;
        $this->entity_manager_interface = $entity_manager_interface;
    }

    public function call(Human $helper, Alert $alert) {   
        $call = new Call;
        $call->setStartCallDate(new \DateTime('now'));

        // Link call with un helper
        $call->setCalledHuman($helper);

        // call with alert
        $call->setAlert($alert);

        $repeat = 0;

        $this->entity_manager_interface->persist($call);
        $this->entity_manager_interface->flush();

        $id = $call->getId();
    
        $account_sid = $this->accountSid;
        $auth_token = $this->auth_token;
        
        $twilio_number = "$this->twilio_number";
        $to_number = $helper->getPhone();
        
        $client = new Client($account_sid, $auth_token);
        $twilioReturn = $client->account->calls->create(  
            $to_number,
            $twilio_number,
            array(
                "method" => "GET",
                "statusCallback" => $this->app_url."/callback",
                "statusCallbackEvent" => array("initiated","answered", "ringing", "completed"),
                "statusCallbackMethod" => "POST",
                "url" => $this->app_url."/ask_question/".$id."/".$repeat,
            )
        );  
        $call->setTwilioReference($twilioReturn->sid);
        $this->entity_manager_interface->persist($call);
        $this->entity_manager_interface->flush();
    }
    
    public function send_sms($alert, $body, Human $recepient) {   
        $sms = new Sms;
        $sms->setSendDate(new \DateTime());
        
        $to_number = $recepient->getPhone();

        // Sms with un human
        $sms->setRecepient($recepient);
        
        // Sms with un alert
        $sms->setAlert($alert);

        $account_sid = $this->accountSid;
        $auth_token = $this->auth_token;

        $twilio_number = $this->twilio_number;
        $client = new Client($account_sid, $auth_token);

        $twilioReturn = $client->messages->create(
            $to_number,
            array(
                'from' => $twilio_number,
                'body' => $body
            )
        );
        $sms->setBody($twilioReturn->body);
        $sms->setTwilioReference($twilioReturn->sid);

        $this->entity_manager_interface->persist($sms);
        $this->entity_manager_interface->flush();    
    } 
}