<?php namespace AppBundle\Service;

class LogService
{
    public function log($text) {
        file_put_contents(
            '/home/wilfried/Bureau/Rangement/Documents/SLP v2/Sos-Lost-People-v2/twilio.log',
            
            date('[Y-m-d H:i:s] ').$text.PHP_EOL ,
            FILE_APPEND | LOCK_EX
        );
    }
}