<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alert
 *
 * @ORM\Table(name="alert")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlertRepository")
 */
class Alert
{

    /**
     * @ORM\OneToMany(targetEntity="Sms", mappedBy="alert")
     */
    private $sms;

    /**
     * @ORM\OneToMany(targetEntity="Call", mappedBy="alert")
     */
    private $calls;

    /**
     * @ORM\ManyToOne(targetEntity="Human", inversedBy="alertsWhereVictim")
     */
    private $victim;

    /**
     * @ORM\ManyToOne(targetEntity="Human", inversedBy="alertsWhereGuardian")
     */
    private $guardian;

    /**
     * @ORM\ManyToMany(targetEntity="Human", inversedBy="alertsWhereHelper")
     */
    private $helpers;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="callFinishDate", type="datetime", nullable=true)
     */
    private $callFinishDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closeAlertDate", type="datetime", nullable=true)
     */
    private $closeAlertDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Alert
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set callFinishDate
     *
     * @param \DateTime $callFinishDate
     *
     * @return Alert
     */
    public function setCallFinishDate($callFinishDate)
    {
        $this->callFinishDate = $callFinishDate;

        return $this;
    }

    /**
     * Get callFinishDate
     *
     * @return \DateTime
     */
    public function getCallFinishDate()
    {
        return $this->callFinishDate;
    }

    /**
     * Set closeAlertDate
     *
     * @param \DateTime $closeAlertDate
     *
     * @return Alert
     */
    public function setCloseAlertDate($closeAlertDate)
    {
        $this->closeAlertDate = $closeAlertDate;

        return $this;
    }

    /**
     * Get closeAlertDate
     *
     * @return \DateTime
     */
    public function getCloseAlertDate()
    {
        return $this->closeAlertDate;
    }
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->calls = new \Doctrine\Common\Collections\ArrayCollection();
        $this->helpers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sms
     *
     * @param \AppBundle\Entity\Sms $sms
     *
     * @return Alert
     */
    public function addSms(\AppBundle\Entity\Sms $sms)
    {
        $this->sms[] = $sms;

        return $this;
    }

    /**
     * Remove sms
     *
     * @param \AppBundle\Entity\Sms $sms
     */
    public function removeSms(\AppBundle\Entity\Sms $sms)
    {
        $this->sms->removeElement($sms);
    }

    /**
     * Get sms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSms()
    {
        return $this->sms;
    }

    /**
     * Add call
     *
     * @param \AppBundle\Entity\Call $call
     *
     * @return Alert
     */
    public function addCall(\AppBundle\Entity\Call $call)
    {
        $this->calls[] = $call;

        return $this;
    }

    /**
     * Remove call
     *
     * @param \AppBundle\Entity\Call $call
     */
    public function removeCall(\AppBundle\Entity\Call $call)
    {
        $this->calls->removeElement($call);
    }

    /**
     * Get calls
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * Set victim
     *
     * @param \AppBundle\Entity\Human $victim
     *
     * @return Alert
     */
    public function setVictim(\AppBundle\Entity\Human $victim = null)
    {
        $this->victim = $victim;
        $victim->addAlertsWhereVictim($this);

        return $this;
    }

    /**
     * Get victim
     *
     * @return \AppBundle\Entity\Human
     */
    public function getVictim()
    {
        return $this->victim;
    }

    /**
     * Set guardian
     *
     * @param \AppBundle\Entity\Human $guardian
     *
     * @return Alert
     */
    public function setGuardian(\AppBundle\Entity\Human $guardian = null)
    {
        $this->guardian = $guardian;
        $guardian->addAlertsWhereGuardian($this);

        return $this;
    }

    /**
     * Get guardian
     *
     * @return \AppBundle\Entity\Human
     */
    public function getGuardian()
    {
        return $this->guardian;
    }

    /**
     * Add helper
     *
     * @param \AppBundle\Entity\Human $helper
     *
     * @return Alert
     */
    public function addHelper(\AppBundle\Entity\Human $helper)
    {
        $this->helpers[] = $helper;

        return $this;
    }

    /**
     * Remove helper
     *
     * @param \AppBundle\Entity\Human $helper
     */
    public function removeHelper(\AppBundle\Entity\Human $helper)
    {
        $this->helpers->removeElement($helper);
    }

    /**
     * Get helpers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHelpers()
    {
        return $this->helpers;
    }
}
