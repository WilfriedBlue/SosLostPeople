<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Call
 *
 * @ORM\Table(name="call")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CallRepository")
 */
class Call
{

    /**
     * @ORM\ManyToOne(targetEntity="Alert", inversedBy="calls")
     */
    private $alert;

    /**
     * @ORM\ManyToOne(targetEntity="Human", inversedBy="calls")
     */
    private $calledHuman;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="answer", type="integer", nullable=true)
     */
    private $answer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startCallDate", type="datetime")
     */
    private $startCallDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="answerCallDate", type="datetime", nullable=true)
     */
    private $answerCallDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endCallDate", type="datetime", nullable=true)
     */
    private $endCallDate;

    /**
     * @var string
     *
     * @ORM\Column(name="twilioReference", type="string", length=255, nullable=true)
     */
    private $twilioReference;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     *
     * @return Call
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return int
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set startCallDate
     *
     * @param \DateTime $startCallDate
     *
     * @return Call
     */
    public function setStartCallDate($startCallDate)
    {
        $this->startCallDate = $startCallDate;

        return $this;
    }

    /**
     * Get startCallDate
     *
     * @return \DateTime
     */
    public function getStartCallDate()
    {
        return $this->startCallDate;
    }

    /**
     * Set answerCallDate
     *
     * @param \DateTime $answerCallDate
     *
     * @return Call
     */
    public function setAnswerCallDate($answerCallDate)
    {
        $this->answerCallDate = $answerCallDate;

        return $this;
    }

    /**
     * Get answerCallDate
     *
     * @return \DateTime
     */
    public function getAnswerCallDate()
    {
        return $this->answerCallDate;
    }

    /**
     * Set endCallDate
     *
     * @param \DateTime $endCallDate
     *
     * @return Call
     */
    public function setEndCallDate($endCallDate)
    {
        $this->endCallDate = $endCallDate;

        return $this;
    }

    /**
     * Get endCallDate
     *
     * @return \DateTime
     */
    public function getEndCallDate()
    {
        return $this->endCallDate;
    }

    /**
     * Set twilioReference
     *
     * @param string $twilioReference
     *
     * @return Call
     */
    public function setTwilioReference($twilioReference)
    {
        $this->twilioReference = $twilioReference;

        return $this;
    }

    /**
     * Get twilioReference
     *
     * @return string
     */
    public function getTwilioReference()
    {
        return $this->twilioReference;
    }


    

    /**
     * Set alert
     *
     * @param \AppBundle\Entity\Alert $alert
     *
     * @return Call
     */
    public function setAlert(\AppBundle\Entity\Alert $alert = null)
    {
        $this->alert = $alert;
        $alert->addCall($this);

        return $this;
    }

    /**
     * Get alert
     *
     * @return \AppBundle\Entity\Alert
     */
    public function getAlert()
    {
        return $this->alert;
    }

    /**
     * Set calledHuman
     *
     * @param \AppBundle\Entity\Human $calledHuman
     *
     * @return Call
     */
    public function setCalledHuman(\AppBundle\Entity\Human $calledHuman = null)
    {
        $this->calledHuman = $calledHuman;
        $calledHuman->addCall($this);

        return $this;
    }

    /**
     * Get calledHuman
     *
     * @return \AppBundle\Entity\Human
     */
    public function getCalledHuman()
    {
        return $this->calledHuman;
    }
}
