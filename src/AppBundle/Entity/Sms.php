<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sms
 *
 * @ORM\Table(name="sms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SmsRepository")
 */
class Sms
{

    /**
     * @ORM\ManyToOne(targetEntity="Alert", inversedBy="sms")
     */
    private $alert;

    /**
     * @ORM\ManyToOne(targetEntity="Human", inversedBy="sms")
     */
    private $recepient;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendDate", type="datetime")
     */
    private $sendDate;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="twilioReference", type="string", length=255)
     */
    private $twilioReference;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sendDate
     *
     * @param \DateTime $sendDate
     *
     * @return Sms
     */
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * Get sendDate
     *
     * @return \DateTime
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Sms
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set twilioReference
     *
     * @param string $twilioReference
     *
     * @return Sms
     */
    public function setTwilioReference($twilioReference)
    {
        $this->twilioReference = $twilioReference;

        return $this;
    }

    /**
     * Get twilioReference
     *
     * @return string
     */
    public function getTwilioReference()
    {
        return $this->twilioReference;
    }

    

    /**
     * Set alert
     *
     * @param \AppBundle\Entity\Alert $alert
     *
     * @return Sms
     */
    public function setAlert(\AppBundle\Entity\Alert $alert = null)
    {
        $this->alert = $alert;
        $alert->addSms($this);

        return $this;
    }

    /**
     * Get alert
     *
     * @return \AppBundle\Entity\Alert
     */
    public function getAlert()
    {
        return $this->alert;
    }

    /**
     * Set recepient
     *
     * @param \AppBundle\Entity\Human $recepient
     *
     * @return Sms
     */
    public function setRecepient(\AppBundle\Entity\Human $recepient = null)
    {
        $this->recepient = $recepient;
        $recepient->addSms($this);
        
        return $this;
    }

    /**
     * Get recepient
     *
     * @return \AppBundle\Entity\Human
     */
    public function getRecepient()
    {
        return $this->recepient;
    }
}
