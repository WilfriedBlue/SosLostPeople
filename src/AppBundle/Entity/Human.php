<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HumanRepository")
 * @ORM\Table(name="human")
 */
class Human extends BaseUser
{

    /**
     * @ORM\OneToMany(targetEntity="Call", mappedBy="calledHuman")
     */
    private $calls;

    /**
     * @ORM\OneToMany(targetEntity="Sms", mappedBy="recepient")
     */
    private $sms;

    /**
     * @ORM\OneToMany(targetEntity="Alert", mappedBy="victim")
     */
    private $alertsWhereVictim;

    /**
     * @ORM\OneToMany(targetEntity="Alert", mappedBy="guardian")
     */
    private $alertsWhereGuardian;

    /**
     * @ORM\ManyToMany(targetEntity="Alert", mappedBy="helpers")
     */
    private $alertsWhereHelper;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var \DateTime
     * @Assert\NotBlank
     *
     * @ORM\Column(name="dateOfBirth", type="datetime")
     */
    private $dateOfBirth;

    /**
     * @var string
     * @Assert\NotBlank
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */

    private $phone;

    /**
     * @var string
     * @Assert\NotBlank
     *
     * @ORM\Column(name="company", type="string", length=255)
     */

    private $company;

    /**
     * @var string
     * @Assert\NotBlank
     *
     * @ORM\Column(name="job", type="string", length=255)
     */

    private $job;

    /**
    * @Assert\NotBlank
    */
    protected $email;


    public function __construct()
    {
        parent::__construct();
        $this->calls = new \Doctrine\Common\Collections\ArrayCollection();
        $this->alertsWhereVictim = new \Doctrine\Common\Collections\ArrayCollection();
        $this->alertsWhereGuardian = new \Doctrine\Common\Collections\ArrayCollection();
        $this->alertsWhereHelper = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sms = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Human
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Human
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return Human
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Human
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Human
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return Human
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }





    /**
     * Add call
     *
     * @param \AppBundle\Entity\Call $call
     *
     * @return Human
     */
    public function addCall(\AppBundle\Entity\Call $call)
    {
        $this->calls[] = $call;

        return $this;
    }

    /**
     * Remove call
     *
     * @param \AppBundle\Entity\Call $call
     */
    public function removeCall(\AppBundle\Entity\Call $call)
    {
        $this->calls->removeElement($call);
    }

    /**
     * Get calls
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * Add sm
     *
     * @param \AppBundle\Entity\Sms $sms
     *
     * @return Human
     */
    public function addSms(\AppBundle\Entity\Sms $sms)
    {
        $this->sms[] = $sms;

        return $this;
    }

    /**
     * Remove sm
     *
     * @param \AppBundle\Entity\Sms $sms
     */
    public function removeSm(\AppBundle\Entity\Sms $sms)
    {
        $this->sms->removeElement($sms);
    }

    /**
     * Get sms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSms()
    {
        return $this->sms;
    }

    /**
     * Add alertsWhereVictim
     *
     * @param \AppBundle\Entity\Alert $alertsWhereVictim
     *
     * @return Human
     */
    public function addAlertsWhereVictim(\AppBundle\Entity\Alert $alertsWhereVictim)
    {
        $this->alertsWhereVictim[] = $alertsWhereVictim;

        return $this;
    }

    /**
     * Remove alertsWhereVictim
     *
     * @param \AppBundle\Entity\Alert $alertsWhereVictim
     */
    public function removeAlertsWhereVictim(\AppBundle\Entity\Alert $alertsWhereVictim)
    {
        $this->alertsWhereVictim->removeElement($alertsWhereVictim);
    }

    /**
     * Get alertsWhereVictim
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlertsWhereVictim()
    {
        return $this->alertsWhereVictim;
    }

    /**
     * Add alertsWhereGuardian
     *
     * @param \AppBundle\Entity\Alert $alertsWhereGuardian
     *
     * @return Human
     */
    public function addAlertsWhereGuardian(\AppBundle\Entity\Alert $alertsWhereGuardian)
    {
        $this->alertsWhereGuardian[] = $alertsWhereGuardian;

        return $this;
    }

    /**
     * Remove alertsWhereGuardian
     *
     * @param \AppBundle\Entity\Alert $alertsWhereGuardian
     */
    public function removeAlertsWhereGuardian(\AppBundle\Entity\Alert $alertsWhereGuardian)
    {
        $this->alertsWhereGuardian->removeElement($alertsWhereGuardian);
    }

    /**
     * Get alertsWhereGuardian
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlertsWhereGuardian()
    {
        return $this->alertsWhereGuardian;
    }

    /**
     * Add alertsWhereHelper
     *
     * @param \AppBundle\Entity\Alert $alertsWhereHelper
     *
     * @return Human
     */
    public function addAlertsWhereHelper(\AppBundle\Entity\Alert $alertsWhereHelper)
    {
        $this->alertsWhereHelper[] = $alertsWhereHelper;
        $alertsWhereHelper->addHelper($this);

        return $this;
    }

    /**
     * Remove alertsWhereHelper
     *
     * @param \AppBundle\Entity\Alert $alertsWhereHelper
     */
    public function removeAlertsWhereHelper(\AppBundle\Entity\Alert $alertsWhereHelper)
    {
        $this->alertsWhereHelper->removeElement($alertsWhereHelper);
    }

    /**
     * Get alertsWhereHelper
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlertsWhereHelper()
    {
        return $this->alertsWhereHelper;
    }

    
    public function setEmail($email)
    {
        $this->email = $email;
        $this->setUsername($email);
        $this->setPassword(" ");
        return $this;
    }
    
}
