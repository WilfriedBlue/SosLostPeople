<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190219131548 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE sms_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE call_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE human_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE alert_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE sms (id INT NOT NULL, alert_id INT DEFAULT NULL, recepient_id INT DEFAULT NULL, sendDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, body VARCHAR(255) NOT NULL, twilioReference VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B0A93A7793035F72 ON sms (alert_id)');
        $this->addSql('CREATE INDEX IDX_B0A93A77F1B7C6C ON sms (recepient_id)');
        $this->addSql('CREATE TABLE call (id INT NOT NULL, alert_id INT DEFAULT NULL, called_human_id INT DEFAULT NULL, answer INT DEFAULT NULL, startCallDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, answerCallDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, endCallDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, twilioReference VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CC8E2F3E93035F72 ON call (alert_id)');
        $this->addSql('CREATE INDEX IDX_CC8E2F3E97A88A30 ON call (called_human_id)');
        $this->addSql('CREATE TABLE human (id INT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, lastName VARCHAR(255) NOT NULL, firstName VARCHAR(255) NOT NULL, dateOfBirth TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, phone VARCHAR(255) NOT NULL, company VARCHAR(255) NOT NULL, job VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A562D5F592FC23A8 ON human (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A562D5F5A0D96FBF ON human (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A562D5F5C05FB297 ON human (confirmation_token)');
        $this->addSql('COMMENT ON COLUMN human.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE alert (id INT NOT NULL, victim_id INT DEFAULT NULL, guardian_id INT DEFAULT NULL, startDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, callFinishDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, closeAlertDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_17FD46C144972A0E ON alert (victim_id)');
        $this->addSql('CREATE INDEX IDX_17FD46C111CC8B0A ON alert (guardian_id)');
        $this->addSql('CREATE TABLE alert_human (alert_id INT NOT NULL, human_id INT NOT NULL, PRIMARY KEY(alert_id, human_id))');
        $this->addSql('CREATE INDEX IDX_4F38221E93035F72 ON alert_human (alert_id)');
        $this->addSql('CREATE INDEX IDX_4F38221E8ABD4580 ON alert_human (human_id)');
        $this->addSql('ALTER TABLE sms ADD CONSTRAINT FK_B0A93A7793035F72 FOREIGN KEY (alert_id) REFERENCES alert (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sms ADD CONSTRAINT FK_B0A93A77F1B7C6C FOREIGN KEY (recepient_id) REFERENCES human (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE call ADD CONSTRAINT FK_CC8E2F3E93035F72 FOREIGN KEY (alert_id) REFERENCES alert (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE call ADD CONSTRAINT FK_CC8E2F3E97A88A30 FOREIGN KEY (called_human_id) REFERENCES human (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert ADD CONSTRAINT FK_17FD46C144972A0E FOREIGN KEY (victim_id) REFERENCES human (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert ADD CONSTRAINT FK_17FD46C111CC8B0A FOREIGN KEY (guardian_id) REFERENCES human (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert_human ADD CONSTRAINT FK_4F38221E93035F72 FOREIGN KEY (alert_id) REFERENCES alert (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert_human ADD CONSTRAINT FK_4F38221E8ABD4580 FOREIGN KEY (human_id) REFERENCES human (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('ALTER TABLE sms DROP CONSTRAINT FK_B0A93A77F1B7C6C');
        $this->addSql('ALTER TABLE call DROP CONSTRAINT FK_CC8E2F3E97A88A30');
        $this->addSql('ALTER TABLE alert DROP CONSTRAINT FK_17FD46C144972A0E');
        $this->addSql('ALTER TABLE alert DROP CONSTRAINT FK_17FD46C111CC8B0A');
        $this->addSql('ALTER TABLE alert_human DROP CONSTRAINT FK_4F38221E8ABD4580');
        $this->addSql('ALTER TABLE sms DROP CONSTRAINT FK_B0A93A7793035F72');
        $this->addSql('ALTER TABLE call DROP CONSTRAINT FK_CC8E2F3E93035F72');
        $this->addSql('ALTER TABLE alert_human DROP CONSTRAINT FK_4F38221E93035F72');
        $this->addSql('DROP SEQUENCE sms_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE call_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE human_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE alert_id_seq CASCADE');
        $this->addSql('DROP TABLE sms');
        $this->addSql('DROP TABLE call');
        $this->addSql('DROP TABLE human');
        $this->addSql('DROP TABLE alert');
        $this->addSql('DROP TABLE alert_human');
    }
}
